﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using xpos334.api.Services;
using xpos334.datamodels;
using xpos334.viewmodels;

namespace xpos334.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiRoleController : ControllerBase
    {
        private readonly XPOS_334Context db;
        private VMResponse respon = new VMResponse();
        private RolesService rolesService;
        private int IdUser = 1;

        public apiRoleController(XPOS_334Context _db)
        {
            db = _db;
            rolesService = new RolesService(db);
        }

        [HttpGet("GetAllData")]
        public List<TblRole> GetAllData()
        {
            List<TblRole> data = db.TblRoles.Where(a => a.IsDelete == false).ToList();
            return data;
        }

        [HttpGet("CheckNameIsExist/{name}/{id}")]
        public bool CheckName(string name, int id)
        {
            TblRole data = new TblRole();
            //if (id == 0)//ini untuk saat create
            //{
            //    data = db.TblRoles.Where(a => a.RoleName == name && a.IsDelete == false).FirstOrDefault();
            //}
            //else //ini untuk saat edit
            //{
            //    data = db.TblRoles.Where(a => a.RoleName == name && a.IsDelete == false && a.Id != id).FirstOrDefault();
            //}

            if (data != null && data.Id != id)
            {
                return true;
            }

            return false;
        }

        [HttpPost("Save")]
        public VMResponse Save(TblRole data)
        {
            data.CreatedBy = IdUser;
            data.CreatedDate = DateTime.Now;
            data.IsDelete = false;

            try
            {
                db.Add(data);
                db.SaveChanges();

                respon.Message = "Data success saved";
            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Failed saved : " + ex.Message;
            }

            return respon;
        }

        [HttpGet("GetDataById/{id}")]
        public TblRole DataById(int id)
        {
            TblRole result = db.TblRoles.Where(a => a.Id == id).FirstOrDefault();
            return result;
        }

        [HttpGet("GetDataById_MenuAccess/{id}")]
        public async Task<VMTblRole> DataById_MenuAccess(int id)
        {
            //TblRole result = db.TblRoles.Where(a => a.Id == id).FirstOrDefault();
            VMTblRole result = db.TblRoles.Where(a => a.Id == id)
                                .Select(a => new VMTblRole()
                                {
                                    Id = a.Id,
                                    RoleName = a.RoleName
                                }).FirstOrDefault()!;
            result.role_menu = await rolesService.GetMenuAccessParentChildByRoleID(result.Id, 0, false);
            return result;
        }

        [HttpPut("Edit")]
        public VMResponse Edit(TblRole data)
        {
            TblRole dt = db.TblRoles.Where(a => a.Id == data.Id).FirstOrDefault();

            if (dt != null)
            {
                dt.RoleName = data.RoleName;
                dt.UpdatedBy = IdUser;
                dt.UpdatedDate = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = "Data success saved";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed saved : " + ex.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not found";
            }

            return respon;
        }

        [HttpPut("Edit_MenuAccess")]
        public VMResponse Edit_MenuAccess(VMTblRole data)
        {
            TblRole dt = db.TblRoles.Where(a => a.Id == data.Id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.RoleName = data.RoleName;
                dt.UpdatedBy = IdUser;
                dt.UpdatedDate = DateTime.Now;

                try
                {
                    db.Update(dt);

                    //SAVE MenuAccess
                    List<TblMenuAccess> roleMenuDB = db.TblMenuAccesses.Where(a => a.IdRole == data.Id).ToList();

                    if (roleMenuDB.Count() > 0)
                    {
                        // delete unused data 
                        List<TblMenuAccess> roleMenuRemove = roleMenuDB.Where(a =>
                        !(data.role_menu.Where(b => b.is_selected && b.IdMenu == a.MenuId).Select(b => b.Id)).Any()
                        ).ToList();
                        foreach (TblMenuAccess item in roleMenuRemove)
                        {
                            //db.Remove(item);

                            item.IsDelete = true;
                            item.UpdatedBy = IdUser;
                            item.UpdatedDate = DateTime.Now;
                            db.Update(item);
                        }

                        // update existing data
                        List<TblMenuAccess> roleMenuUpdate = roleMenuDB.Where(a =>
                        (data.role_menu.Where(b => b.is_selected && b.IdMenu == a.MenuId).Select(b => b.Id)).Any()
                        ).ToList();
                        foreach (TblMenuAccess item in roleMenuUpdate)
                        {
                            if (item.IsDelete == true)
                            {
                                item.IsDelete = false;
                                item.UpdatedBy = IdUser;
                                item.UpdatedDate = DateTime.Now;
                                db.Update(item);
                            }
                        }
                    }

                    // insert new data role menu
                    List<TblMenuAccess> roleMenuAdd = data.role_menu.Where(a => 
                    !(roleMenuDB.Where(b => b.MenuId == a.IdMenu).Select(b => b.Id)).Any() && a.is_selected
                        ).Select(a => new TblMenuAccess()
                        {
                            MenuId = a.IdMenu,
                            IdRole = data.Id,
                            IsDelete = false,
                            CreatedBy = IdUser,
                            CreatedDate = DateTime.Now
                        }).ToList();
                    foreach (TblMenuAccess item in roleMenuAdd)
                    {
                        db.Add(item);
                    }

                    db.SaveChanges();

                    respon.Message = "Data success saved";
                }
                catch (Exception e)
                {
                    respon.Success = false;
                    respon.Message = "Failed saved : " + e.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not found";
            }

            return respon;
        }

        //INI CARA : UPDATE ISDELETE TRUE (ALL)  & INSERT (ALL) 
        /*[HttpPut("Edit_MenuAccess")]
        public VMResponse Edit_MenuAccess(VMTblRole data)
        {
            TblRole dt = db.TblRoles.Where(a => a.Id == data.Id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.RoleName = data.RoleName;
                dt.UpdatedBy = IdUser;
                dt.UpdatedDate = DateTime.Now;

                try
                {
                    db.Update(dt);

                    //SAVE MenuAccess
                    if (data.role_menu.Count() > 0)
                    {
                        //Remove MenuAccess
                        List<TblMenuAccess> ListMenuAccessRemove = db.TblMenuAccesses.Where(a => a.IdRole == data.Id).ToList();
                        if (ListMenuAccessRemove.Count() > 0)
                        {
                            foreach (TblMenuAccess item in ListMenuAccessRemove)
                            {
                                item.IsDelete = true;
                                item.UpdatedBy = IdUser;
                                item.UpdatedDate = DateTime.Now;
                                db.Update(item);
                            }
                        }

                        //Insert MenuAccess
                        List<TblMenuAccess> ListMenuAccessAdd = data.role_menu.Where(a => a.is_selected == true)
                                                                .Select(a => new TblMenuAccess()
                                                                {
                                                                    IdRole = data.Id,
                                                                    MenuId = a.IdMenu,
                                                                    IsDelete = false,
                                                                    CreatedBy = IdUser,
                                                                    CreatedDate = DateTime.Now

                                                                }).ToList();

                        foreach (TblMenuAccess item in ListMenuAccessAdd)
                        {
                            db.Add(item);
                        }
                    }

                    db.SaveChanges();

                    respon.Message = "Data success saved";
                }
                catch (Exception e)
                {
                    respon.Success = false;
                    respon.Message = "Failed saved : " + e.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not found";
            }

            return respon;
        }*/

        [HttpDelete("Delete/{id}")]
        public VMResponse Delete(int id)
        {
            TblRole dt = db.TblRoles.Where(a => a.Id == id).FirstOrDefault();

            if (dt != null)
            {
                dt.IsDelete = true;
                dt.UpdatedBy = IdUser;
                dt.UpdatedDate = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = "Data success deleted";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed deleted : " + ex.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not found";
            }

            return respon;
        }


    }
}
